<?php
/**
 * Create notyfication from system
 */


function woocommerce_order_status_add_notify($order_id, $old_status, $new_status) {
  global $woocommerce;
  $option = get_option( 'nsfa_option' );
  $order = new WC_Order( $order_id );
  nsfa_add_notify(array(
    'from' => 'shop',
    'to' => $option['administrator_id'].','.$order->get_user_id(),
    'message' => '#' . $order->get_order_number() . ' zmieniło status na ' . wc_get_order_status_name($order->get_status()),
    'url' => $order->get_view_order_url() ,
  ));
}
add_action( 'woocommerce_order_status_changed', 'woocommerce_order_status_add_notify',10,3);

function woocommerce_new_order_status_add_notify( $order_id, $posted ){
  global $woocommerce;
  $option = get_option( 'nsfa_option' );
  $order = new WC_Order( $order_id );
  nsfa_add_notify(array(
    'from' => 'shop',
    'to' => $option['administrator_id'].','.$order->get_user_id(),
    'message' => 'Nowe zamówienie #' . $order->get_order_number(),
    'url' => $order->get_view_order_url() ,
  ));

}

add_action( 'woocommerce_checkout_order_processed', 'woocommerce_new_order_status_add_notify', 15,2);
