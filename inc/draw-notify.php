<?php
function recent_notifications_draw_html(){
  $notyfications = nsfa_get_notify();
  echo '<!-- notify dropdown start-->
  <li id="header-notifications-orders" class="dropdown">
      <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
          <i class="fa fa fa-bell-o"></i>
          <span class="badge bg-theme notify-count"></span>
      </a>
      <ul class="dropdown-menu extended inbox">
        <div class="notify-arrow notify-arrow-green"></div>
        <li class="green">
            <p class="head"></p><button class="mark-all btn btn-danger"><i class="fa fa fa-trash-o"></i></button>
        </li>
        <div class="scroll-y messages">
        </div>
      </ul>
  </li>
  <script>
    var headerNotyficationsTopOnce = "'.wp_create_nonce( 'nsfa-header-notyfications-once' ).'";
  </script>
  <!-- notify dropdown end -->';
}

add_action( 'notification_bar', 'recent_notifications_draw_html' , 10);
