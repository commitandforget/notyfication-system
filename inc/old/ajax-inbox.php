<?php
/*
 * Ajax functions
 */

//Nav
function nsfa_nav_tabs(){
	?>
	<ul class="nav nav-tabs showback-tabs">
	  <li role="presentation" class="<?php echo ('nsfa_inbox' == $_GET['page']) ? 'active' : '' ?>"><a href="?page=nsfa_inbox"><?php _e( 'Inbox', 'ns4a' ); ?></a></li>
	  <li role="presentation" class="<?php echo ('nsfa_outbox' == $_GET['page']) ? 'active' : '' ?>"><a href="?page=nsfa_outbox"><?php _e('Outbox \ View Message', 'ns4a'); ?></a></li>
	  <li role="presentation" class="<?php echo ('nsfa_send' == $_GET['page']) ? 'active' : '' ?>"><a href="?page=nsfa_send"><?php _e( 'Send Private Message', 'ns4a' ); ?></a></li>
	</ul>
	<?php
}

function nsfa_main_wrapper(){
	switch $_GET['page'] {
		case 'nsfa_inbox':

			break;

	}

}
 // Return inbox
function nsfa_display_messages(){
	global $wpdb, $current_user;
	// show all messages which have not been deleted by this user (deleted status != 2)
	$msgs = $wpdb->get_results( 'SELECT `id`, `sender`, `subject`, `read`, `date` FROM ' . $wpdb->prefix . 'ns WHERE `recipient` = "' . $current_user->user_login . '" AND `deleted` != "2" ORDER BY `date` DESC' );
	if ( !empty( $status ) )
	{
		echo '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', $status, '</div>';
	}
	?>
	<div class="showback">
			<h3><i class="fa fa-angle-right"></i> <?php _e( 'Inbox', 'ns4a' ); ?></h3>
		<?php
		if ( empty( $msgs ) )
		{
			echo '<p>', __( 'You have no items in inbox.', 'ns4a' ), '</p>';
		}
		else
		{
			$n = count( $msgs );
			$num_unread = 0;
			foreach ( $msgs as $msg )
			{
				if ( !( $msg->read ) )
				{
					$num_unread++;
				}
			}
			echo '<p>', sprintf( _n( 'You have %d private message (%d unread).', 'You have %d private messages (%d unread).', $n, 'ns4a' ), $n, $num_unread ), '</p>';
			?>
			<form action="" method="get">
				<?php wp_nonce_field( 'rwpm-bulk-action_inbox' ); ?>
				<input type="hidden" name="page" value="nsfa_inbox" />

				<div class="input-group input-group-sm col-md-3">
					<select name="action" class="form-control">
						<option value="-1" selected="selected"><?php _e( 'Bulk Action', 'ns4a' ); ?></option>
						<option value="delete"><?php _e( 'Delete', 'ns4a' ); ?></option>
						<option value="mar"><?php _e( 'Mark As Read', 'ns4a' ); ?></option>
					</select>
					<span class="input-group-btn">
		        <input type="submit" class="btn btn-default" value="<?php _e( 'Apply', 'ns4a' ); ?>" />
		      </span>
				</div>
				<section id="no-more-tables">
				<table class="table table-hover table-striped table-condensed cf" cellspacing="0">
					<thead>
					<tr>
						<th class="manage-column check-column" width="3%"><input type="checkbox" /></th>
						<th class="manage-column" width="10%"><?php _e( 'Sender', 'ns4a' ); ?></th>
						<th class="manage-column"><?php _e( 'Subject', 'ns4a' ); ?></th>
						<th class="manage-column" width="20%"><?php _e( 'Date', 'ns4a' ); ?></th>
					</tr>
					</thead>
					<tbody>
						<?php
						foreach ( $msgs as $msg )
						{
							$msg->sender = $wpdb->get_var( "SELECT display_name FROM $wpdb->users WHERE user_login = '$msg->sender'" );
							?>
						<tr class="<?php echo (!( $msg->read )) ? 'info' : '' ?>" >
							<th class="check-column"><input type="checkbox" name="id[]" value="<?php echo $msg->id; ?>" />
							</th>
							<td><?php echo $msg->sender; ?></td>
							<td>
								<?php
								if ( $msg->read )
								{
									echo '<a href="', wp_nonce_url( "?page=nsfa_inbox&action=view&id=$msg->id", 'rwpm-view_inbox_msg_' . $msg->id ), '">', stripcslashes( $msg->subject ), '</a>';
								}
								else
								{
									echo '<a href="', wp_nonce_url( "?page=nsfa_inbox&action=view&id=$msg->id", 'rwpm-view_inbox_msg_' . $msg->id ), '"><b>', stripcslashes( $msg->subject ), '</b></a>';
								}
								/*
								?>
								<div class="row-actions">
									<span>
										<a href="<?php echo wp_nonce_url( "?page=nsfa_inbox&action=view&id=$msg->id", 'rwpm-view_inbox_msg_' . $msg->id ); ?>"><?php _e( 'View', 'ns4a' ); ?></a>
									</span>
									<?php
									if ( !( $msg->read ) )
									{
										?>
										<span>
											| <a href="<?php echo wp_nonce_url( "?page=nsfa_inbox&action=mar&id=$msg->id", 'rwpm-mar_inbox_msg_' . $msg->id ); ?>"><?php _e( 'Mark As Read', 'ns4a' ); ?></a>
										</span>
										<?php

									}
									?>
									<span class="delete">
										| <a class="delete"	href="<?php echo wp_nonce_url( "?page=nsfa_inbox&action=delete&id=$msg->id", 'rwpm-delete_inbox_msg_' . $msg->id ); ?>"><?php _e( 'Delete', 'ns4a' ); ?></a>
									</span>
									<span class="reply">
										| <a class="reply"
										href="<?php echo wp_nonce_url( "?page=nsfa_send&recipient=$msg->sender&id=$msg->id&subject=Re: " . stripcslashes( $msg->subject ), 'rwpm-reply_inbox_msg_' . $msg->id ); ?>"><?php _e( 'Reply', 'ns4a' ); ?></a>
									</span>
								</div>
								<?php */ ?>
							</td>
							<td><?php echo $msg->date; ?></td>
						</tr>
							<?php

						}
						?>
					</tbody>

				</table>
				</section>
			</form>
			<?php

		}
		?>
	</div>
	<?php
}
function nsfa_open_message(){
  global $wpdb, $current_user;

	// if view message
	if ( isset( $_GET['action'] ) && 'view' == $_GET['action'] && !empty( $_GET['id'] ) )
	{
		$id = $_GET['id'];

		check_admin_referer( "rwpm-view_inbox_msg_$id" );

		// mark message as read
		$wpdb->update( $wpdb->prefix . 'ns', array( 'read' => 1 ), array( 'id' => $id ) );

		// select message information
		$msg = $wpdb->get_row( 'SELECT * FROM ' . $wpdb->prefix . 'ns WHERE `id` = "' . $id . '" LIMIT 1' );
		$msg->sender = $wpdb->get_var( "SELECT display_name FROM $wpdb->users WHERE user_login = '$msg->sender'" );
		?>
    <div class="showback">
			<h3><i class="fa fa-angle-right"></i> <?php echo stripcslashes( $msg->subject ); ?></h3>
      <div class="btn-group" role="group" aria-label="...">
        <a class="btn btn-danger" href="<?php echo wp_nonce_url( "?page=nsfa_inbox&action=delete&id=$msg->id", 'rwpm-delete_inbox_msg_' . $msg->id ); ?>">
          <i class="fa fa-trash-o fa-lg"></i><?php _e( 'Delete', 'ns4a' ); ?>
        </a>
        <a class="btn btn-primary"  href="<?php echo wp_nonce_url( "?page=nsfa_send&recipient=$msg->sender&id=$msg->id&subject=Re: " . stripcslashes( $msg->subject ), 'rwpm-reply_inbox_msg_' . $msg->id ); ?>">
          <i class="fa fa-reply"></i><?php _e( 'Reply', 'ns4a' ); ?>
        </a>
      </div>
      <p><?php printf( __( '<b>Sender</b>: %s<br /><b>Date</b>: %s', 'ns4a' ), $msg->sender, $msg->date ); ?></p>

      <div class="well"><?php echo nl2br( stripcslashes( $msg->content ) ); ?></div>
      <a class="btn btn-primary" href="<?php echo wp_nonce_url( "?page=nsfa_send&recipient=$msg->sender&id=$msg->id&subject=Re: " . stripcslashes( $msg->subject ), 'rwpm-reply_inbox_msg_' . $msg->id ); ?>">
        <i class="fa fa-reply"></i><?php _e( 'Reply', 'ns4a' ); ?>
      </a>
		</div>
		<?php
		// don't need to do more!
		return;
	}
}
function nsfa_mark_as_read(){
  global $wpdb, $current_user;

  // if mark messages as read
	if ( isset( $_GET['action'] ) && 'mar' == $_GET['action'] && !empty( $_GET['id'] ) )
	{
		$id = $_GET['id'];

		if ( !is_array( $id ) )
		{
			check_admin_referer( "rwpm-mar_inbox_msg_$id" );
			$id = array( $id );
		}
		else
		{
			check_admin_referer( "rwpm-bulk-action_inbox" );
		}
		$n = count( $id );
		$id = implode( ',', $id );
		if ( $wpdb->query( 'UPDATE ' . $wpdb->prefix . 'ns SET `read` = "1" WHERE `id` IN (' . $id . ')' ) )
		{
			$status = _n( 'Message marked as read.', 'Messages marked as read', $n, 'ns4a' );
		}
		else
		{
			$status = __( 'Error. Please try again.', 'ns4a' );
		}
	}
}
function nsfa_delete_message(){
  global $wpdb, $current_user;

  // if delete message
	if ( isset( $_GET['action'] ) && 'delete' == $_GET['action'] && !empty( $_GET['id'] ) )
	{
		$id = $_GET['id'];

		if ( !is_array( $id ) )
		{
			check_admin_referer( "rwpm-delete_inbox_msg_$id" );
			$id = array( $id );
		}
		else
		{
			check_admin_referer( "rwpm-bulk-action_inbox" );
		}

		$error = false;
		foreach ( $id as $msg_id )
		{
			// check if the sender has deleted this message
			$sender_deleted = $wpdb->get_var( 'SELECT `deleted` FROM ' . $wpdb->prefix . 'ns WHERE `id` = "' . $msg_id . '" LIMIT 1' );

			// create corresponding query for deleting message
			if ( $sender_deleted == 1 )
			{
				$query = 'DELETE from ' . $wpdb->prefix . 'ns WHERE `id` = "' . $msg_id . '"';
			}
			else
			{
				$query = 'UPDATE ' . $wpdb->prefix . 'ns SET `deleted` = "2" WHERE `id` = "' . $msg_id . '"';
			}

			if ( !$wpdb->query( $query ) )
			{
				$error = true;
			}
		}
		if ( $error )
		{
			$status = __( 'Error. Please try again.', 'ns4a' );
		}
		else
		{
			$status = _n( 'Message deleted.', 'Messages deleted.', count( $id ), 'ns4a' );
		}
	}
}
function nsfa_send_message(){
	global $wpdb, $current_user;
	$option = get_option( 'nsfa_option' );
	if ( $_REQUEST['page'] == 'nsfa_send' && isset( $_POST['submit'] ) ){
  ?>
  <div class="wrap">
    <h2><?php _e( 'Send Private Message', 'ns4a' ); ?></h2>
    <?php

		$error = false;
		$status = array();

		// Check if total pm of current user exceed limit
		$role = $current_user->roles[0];
		$sender = $current_user->user_login;
		$total = $wpdb->get_var( 'SELECT COUNT(*) FROM ' . $wpdb->prefix . 'ns WHERE `sender` = "' . $sender . '" OR `recipient` = "' . $sender . '"' );
		if ( ( $option[$role] != 0 ) && ( $total >= $option[$role] ) )
		{
			$error = true;
			$status[] = __( 'You have exceeded the limit of mailbox. Please delete some messages before sending another.', 'ns4a' );
		}

		// Get input fields with no html tags and all are escaped
		$subject = strip_tags( $_POST['subject'] );
		$content = $_POST['content'] ;
		$recipient = $option['type'] == 'autosuggest' ? explode( ',', $_POST['recipient'] ) : $_POST['recipient'];
		$recipient = array_map( 'strip_tags', $recipient );

		// Allow to filter content
		$content = apply_filters( 'nsfa_content_send', $content );

		// Remove slash automatically in wp
		$subject = stripslashes( $subject );
		$content = stripslashes( $content );
		$recipient = array_map( 'stripslashes', $recipient );

		// Escape sql
		$subject = esc_sql( $subject );
		$content = esc_sql( $content );
		$recipient = array_map( 'esc_sql', $recipient );

		// Remove duplicate and empty recipient
		$recipient = array_unique( $recipient );
		$recipient = array_filter( $recipient );

		// Check input fields
		if ( empty( $recipient ) )
		{
			$error = true;
			$status[] = __( 'Please enter username of recipient.', 'ns4a' );
		}
		if ( empty( $subject ) )
		{
			$error = true;
			$status[] = __( 'Please enter subject of message.', 'ns4a' );
		}
		if ( empty( $content ) )
		{
			$error = true;
			$status[] = __( 'Please enter content of message.', 'ns4a' );
		}

		if ( !$error )
		{
			$numOK = $numError = 0;
			foreach ( $recipient as $rec )
			{
				// get user_login field
				$rec = $wpdb->get_var( "SELECT user_login FROM $wpdb->users WHERE display_name = '$rec' LIMIT 1" );
				$new_message = array(
					'id'        => NULL,
					'subject'   => $subject,
					'content'   => $content,
					'sender'    => $sender,
					'recipient' => $rec,
					'date'      => current_time( 'mysql' ),
					'read'      => 0,
					'deleted'   => 0
				);
				// insert into database
				if ( $wpdb->insert( $wpdb->prefix . 'ns', $new_message, array( '%d', '%s', '%s', '%s', '%s', '%s', '%d', '%d' ) ) )
				{
					$numOK++;
					unset( $_REQUEST['recipient'], $_REQUEST['subject'], $_REQUEST['content'] );

					// send email to user
					if ( $option['email_enable'] )
					{
						$sender = $wpdb->get_var( "SELECT display_name FROM $wpdb->users WHERE user_login = '$sender' LIMIT 1" );

						// replace tags with values
						$tags = array( '%BLOG_NAME%', '%BLOG_ADDRESS%', '%SENDER%', '%INBOX_URL%' );
						$replacement = array( get_bloginfo( 'name' ), get_bloginfo( 'admin_email' ), $sender, admin_url( 'admin.php?page=nsfa_inbox' ) );

						$email_name = str_replace( $tags, $replacement, $option['email_name'] );
						$email_address = str_replace( $tags, $replacement, $option['email_address'] );
						$email_subject = str_replace( $tags, $replacement, $option['email_subject'] );
						$email_body = str_replace( $tags, $replacement, $option['email_body'] );

						// set default email from name and address if missed
						if ( empty( $email_name ) )
							$email_name = get_bloginfo( 'name' );

						if ( empty( $email_address ) )
							$email_address = get_bloginfo( 'admin_email' );

						$email_subject = strip_tags( $email_subject );
						if ( get_magic_quotes_gpc() )
						{
							$email_subject = stripslashes( $email_subject );
							$email_body = stripslashes( $email_body );
						}
						$email_body = nl2br( $email_body );

						$recipient_email = $wpdb->get_var( "SELECT user_email from $wpdb->users WHERE display_name = '$rec'" );
						$mailtext = "<html><head><title>$email_subject</title></head><body>$email_body</body></html>";

						// set headers to send html email
						$headers = "To: $recipient_email\r\n";
						$headers .= "From: $email_name <$email_address>\r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= 'Content-Type: ' . get_bloginfo( 'html_type' ) . '; charset=' . get_bloginfo( 'charset' ) . "\r\n";

						wp_mail( $recipient_email, $email_subject, $mailtext, $headers );
					}
				}
				else
				{
					$numError++;
				}
			}

			$status[] = sprintf( _n( '%d message sent.', '%d messages sent.', $numOK, 'ns4a' ), $numOK ) . ' ' . sprintf( _n( '%d error.', '%d errors.', $numError, 'ns4a' ), $numError );
		}

		echo '<div id="message" class="updated fade"><p>', implode( '</p><p>', $status ), '</p></div>';
	}
	?>
	<?php do_action( 'nsfa_before_form_send' ); ?>
    <form method="post" action="" id="send-form" enctype="multipart/form-data">
	    <input type="hidden" name="page" value="nsfa_send" />
        <table class="form-table">
            <tr>
                <th><?php _e( 'Recipient', 'ns4a' ); ?></th>
                <td>
					<?php
					// if message is not sent (by errors) or in case of replying, all input are saved

					$recipient = !empty( $_POST['recipient'] ) ? $_POST['recipient'] : ( !empty( $_GET['recipient'] )
						? $_GET['recipient'] : '' );

					// strip slashes if needed
					$subject = isset( $_REQUEST['subject'] ) ? ( get_magic_quotes_gpc() ? stripcslashes( $_REQUEST['subject'] )
						: $_REQUEST['subject'] ) : '';
					$subject = urldecode( $subject );  // for some chars like '?' when reply

					if ( empty( $_GET['id'] ) )
					{
						$content = isset( $_REQUEST['content'] ) ?  $_REQUEST['content']  : '';
					}
					else
					{
						$id = $_GET['id'];
						$msg = $wpdb->get_row( 'SELECT * FROM ' . $wpdb->prefix . 'ns WHERE `id` = "' . $id . '" LIMIT 1' );

						$content = '<p>&nbsp;</p>';
						$content .= '<p>---</p>';
						$content .= '<p><em>' . __( 'In: ', 'ns4a' ) . $msg->date . "\t" . $msg->sender . __( ' Wrote:', 'ns4a' ) . '</em></p>';
						$content .= wpautop( $msg->content );
						$content  = stripslashes( $content );
					}
					// if auto suggest feature is turned on
					if ( $option['type'] == 'autosuggest' )
					{
						?>
                        <input id="recipient" type="text" name="recipient" class="large-text" />
						<?php

					}
					else // classic way: select recipient from dropdown list
					{
						// Get all users of blog
						$args = array(
							'order'   => 'ASC',
							'orderby' => 'display_name' );
						$values = get_users( $args );
						$values = apply_filters( 'nsfa_recipients', $values );
						?>
						<select name="recipient[]" multiple="multiple" size="5">
							<?php
							foreach ( $values as $value )
							{
								$selected = ( $value->display_name == $recipient ) ? ' selected="selected"' : '';
								echo "<option value='$value->display_name'$selected>$value->display_name</option>";
							}
							?>
						</select>
						<?php
					}
					?>
                </td>
            </tr>
            <tr>
                <th><?php _e( 'Subject', 'ns4a' ); ?></th>
                <td><input type="text" name="subject" value="<?php echo $subject; ?>" class="large-text" /></td>
            </tr>
            <tr>
                <th><?php _e( 'Content', 'ns4a' ); ?></th>
                <th><?php  wp_editor( $content, 'rw-text-editor', $settings = array( 'textarea_name' => 'content' ) );?></th>
            </tr>
	        <?php do_action( 'nsfa_form_send' ); ?>
        </table>
	    <p class="submit"><input type="submit" value="Send" class="button-primary" id="submit" name="submit"></p>
    </form>
	<?php do_action( 'nsfa_after_form_send' ); ?>
	</div>
	<?php
}

add_action( 'main_inbox', 'nsfa_nav_tabs' );
add_action( 'main_inbox', 'nsfa_display_messages' );
add_action( 'main_inbox', 'nsfa_open_message' );
add_action( 'main_inbox', 'nsfa_mark_as_read' );
add_action( 'main_inbox', 'nsfa_delete_message' );
