<?php
/**
 * Inbox page
 */
function nsfa_inbox()
{
	global $wpdb, $current_user;

	// if view message
	if ( isset( $_GET['action'] ) && 'view' == $_GET['action'] && !empty( $_GET['id'] ) )
	{
		$id = $_GET['id'];

		check_admin_referer( "rwpm-view_inbox_msg_$id" );

		// mark message as read
		$wpdb->update( $wpdb->prefix . 'ns', array( 'read' => 1 ), array( 'id' => $id ) );

		// select message information
		$msg = $wpdb->get_row( 'SELECT * FROM ' . $wpdb->prefix . 'ns WHERE `id` = "' . $id . '" LIMIT 1' );
		$msg->sender = $wpdb->get_var( "SELECT display_name FROM $wpdb->users WHERE user_login = '$msg->sender'" );
		?>
			<a href="?page=nsfa_inbox" class="btn btn-link btn-sm"><i class="fa fa-angle-left"></i> <?php _e('Back to outbox', 'ns4a'); ?></a>
			<h3><i class="fa fa-angle-right"></i> <?php echo stripcslashes( $msg->subject ); ?></h3>
      <p><?php printf( __( '<b>Sender</b>: %s<br /><b>Date</b>: %s', 'ns4a' ), $msg->sender, $msg->date ); ?></p>

      <div class="well message-wrapper"><?php echo nl2br( stripcslashes( $msg->content ) ); ?></div>
			<div class="btn-group" role="group" aria-label="...">
        <a class="btn btn-danger" href="<?php echo wp_nonce_url( "?page=nsfa_inbox&action=delete&id=$msg->id", 'rwpm-delete_inbox_msg_' . $msg->id ); ?>">
          <i class="fa fa-trash-o fa-lg"></i><?php _e( 'Delete', 'ns4a' ); ?>
        </a>
        <a class="btn btn-primary"  href="<?php echo wp_nonce_url( "?page=nsfa_send&recipient=$msg->sender&id=$msg->id&subject=Re: " . stripcslashes( $msg->subject ), 'rwpm-reply_inbox_msg_' . $msg->id ); ?>">
          <i class="fa fa-reply"></i><?php _e( 'Reply', 'ns4a' ); ?>
        </a>
      </div>
		<?php
		// don't need to do more!
		return;
	}

	// if mark messages as read
	if ( isset( $_GET['action'] ) && 'mar' == $_GET['action'] && !empty( $_GET['id'] ) )
	{
		$id = $_GET['id'];

		if ( !is_array( $id ) )
		{
			check_admin_referer( "rwpm-mar_inbox_msg_$id" );
			$id = array( $id );
		}
		else
		{
			check_admin_referer( "rwpm-bulk-action_inbox" );
		}
		$n = count( $id );
		$id = implode( ',', $id );
		if ( $wpdb->query( 'UPDATE ' . $wpdb->prefix . 'ns SET `read` = "1" WHERE `id` IN (' . $id . ')' ) )
		{
			$status = array('content' => _n( 'Message marked as read.', 'Messages marked as read', $n, 'ns4a' ), 'type' => 'info');
		}
		else
		{
			$status = array('content' => __( 'Error. Please try again.', 'ns4a' ), 'type' => 'danger');
		}
	}

	// if delete message
	if ( isset( $_GET['action'] ) && 'delete' == $_GET['action'] && !empty( $_GET['id'] ) )
	{
		$id = $_GET['id'];

		if ( !is_array( $id ) )
		{
			check_admin_referer( "rwpm-delete_inbox_msg_$id" );
			$id = array( $id );
		}
		else
		{
			check_admin_referer( "rwpm-bulk-action_inbox" );
		}

		$error = false;
		foreach ( $id as $msg_id )
		{
			// check if the sender has deleted this message
			$sender_deleted = $wpdb->get_var( 'SELECT `deleted` FROM ' . $wpdb->prefix . 'ns WHERE `id` = "' . $msg_id . '" LIMIT 1' );

			// create corresponding query for deleting message
			if ( $sender_deleted == 1 )
			{
				$query = 'DELETE from ' . $wpdb->prefix . 'ns WHERE `id` = "' . $msg_id . '"';
			}
			else
			{
				$query = 'UPDATE ' . $wpdb->prefix . 'ns SET `deleted` = "2" WHERE `id` = "' . $msg_id . '"';
			}

			if ( !$wpdb->query( $query ) )
			{
				$error = true;
			}
		}
		if ( $error )
		{
			$status = array('content' => __( 'Error. Please try again.', 'ns4a' ), 'type' => 'danger');
		}
		else
		{
			$status = array('content' => _n( 'Message deleted.', 'Messages deleted.', count( $id ), 'ns4a' ), 'type' => 'success');
		}
	}

	// show all messages which have not been deleted by this user (deleted status != 2)
	$msgs = $wpdb->get_results( 'SELECT `id`, `sender`, `subject`, `read`, `date` FROM ' . $wpdb->prefix . 'ns WHERE `recipient` = "' . $current_user->user_login . '" AND `deleted` != "2" ORDER BY `date` DESC' );

	if (!empty($status)) {
			echo '<div class="alert alert-'.$status['type'].' alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				', $status['content'], '</div>';
	}
	if ( empty( $msgs ) )
	{
		echo '<h3><i class="fa fa-angle-right"></i> '. __( 'Inbox', 'ns4a' ) .'</h3>';
		echo '<p>', __( 'You have no items in inbox.', 'ns4a' ), '</p>';
	}
	else
	{
		$n = count( $msgs );
		$num_unread = 0;
		foreach ( $msgs as $msg )
		{
			if ( !( $msg->read ) )
			{
				$num_unread++;
			}
		}
		?>

		<form action="" method="get">
			<?php wp_nonce_field( 'rwpm-bulk-action_inbox' ); ?>
			<input type="hidden" name="page" value="nsfa_inbox" />
			<div class="row">
				<div class="col-md-9">
					<h3><i class="fa fa-angle-right"></i> <?php _e( 'Inbox', 'ns4a' ); ?> <span class="badge"><?php echo $num_unread; ?></span></h3>
				</div>
				<div class="col-md-3 h3">
					<div class="input-group input-group-sm bulk-action_inbox">
						<select name="action" class="form-control">
							<option value="-1" selected="selected"><?php _e( 'Bulk Action', 'ns4a' ); ?></option>
							<option value="delete"><?php _e( 'Delete', 'ns4a' ); ?></option>
							<option value="mar"><?php _e( 'Mark As Read', 'ns4a' ); ?></option>
						</select>
						<span class="input-group-btn">
							<input type="submit" class="btn btn-default" value="<?php _e( 'Apply', 'ns4a' ); ?>" />
						</span>
					</div>
				</div>
			</div>

			<section id="no-more-tables">
				<table class="table table-hover table-striped table-condensed cf" cellspacing="0">
					<thead>
					<tr>
						<th class="manage-column check-column" width="3%" ><input type="checkbox" /></th>
						<th class="manage-column" width="10%"><?php _e( 'Sender', 'ns4a' ); ?></th>
						<th class="manage-column" ><?php _e( 'Subject', 'ns4a' ); ?></th>
						<th class="manage-column" width="15%"><?php _e( 'Date', 'ns4a' ); ?></th>
						<th class="manage-column" width="30%"></th>
					</tr>
					</thead>
					<tbody>
						<?php
						foreach ( $msgs as $msg )
						{
							$msg->sender = $wpdb->get_var( "SELECT display_name FROM $wpdb->users WHERE user_login = '$msg->sender'" );
							?>
						<tr <?php echo ( !$msg->read ) ? 'class="success"' : ''; ?>>
							<td data-title="Zaznacz" class="check-column"><input type="checkbox" name="id[]" value="<?php echo $msg->id; ?>" />
							</td>
							<td data-title="<?php _e( 'Sender', 'ns4a' ); ?>" ><?php echo $msg->sender; ?></td>
							<td data-title="<?php _e( 'Subject', 'ns4a' ); ?>" >
								<?php
									echo '<a href="', wp_nonce_url( "?page=nsfa_inbox&action=view&id=$msg->id", 'rwpm-view_inbox_msg_' . $msg->id ), '">', stripcslashes( $msg->subject ), '</a>';
								?>
							</td>
							<td data-title="<?php _e( 'Date', 'ns4a' ); ?>" ><?php echo $msg->date; ?></td>
							<td class="actions-row" >
									<?php
									if ( !( $msg->read ) )
									{
										?>
										<a href="<?php echo wp_nonce_url( "?page=nsfa_inbox&action=mar&id=$msg->id", 'rwpm-mar_inbox_msg_' . $msg->id ); ?>" class="btn btn-success btn-xs"><i class="fa fa-check"></i> <?php _e( 'Mark As Read', 'ns4a' ); ?></a>
										<?php
									}
									?>
									<a href="<?php echo wp_nonce_url( "?page=nsfa_inbox&action=delete&id=$msg->id", 'rwpm-delete_inbox_msg_' . $msg->id ); ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i><?php _e( 'Delete', 'ns4a' ); ?></a>
									<a href="<?php echo wp_nonce_url( "?page=nsfa_send&recipient=$msg->sender&id=$msg->id&subject=Re: " . stripcslashes( $msg->subject ), 'rwpm-reply_inbox_msg_' . $msg->id ); ?>" class="btn btn-primary btn-xs"><i class="fa fa-reply "></i><?php _e( 'Reply', 'ns4a' ); ?></a>
							</td>
						</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</section>
			<span> wszystkich: <?php echo $n; ?></span>
		</form>
		<?php
	}
	?>
	</div>
	<?php
}
