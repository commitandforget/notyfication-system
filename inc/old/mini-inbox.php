<?php
function nsfa_get_mini_inbox($max = 5){
  global $wpdb, $current_user;

  if ( !is_user_logged_in() )
  {
    return;
  }
  $return_array = array(
    'unreaded' => '',
    'messages_count' => '',
    'messages' => array(),
  );
  // get number of PM
  $num_pm = $wpdb->get_var( 'SELECT COUNT(*) FROM ' . $wpdb->prefix . 'ns WHERE `recipient` = "' . $current_user->user_login . '" AND `deleted` != "2"' );
  $num_unread = $wpdb->get_var( 'SELECT COUNT(*) FROM ' . $wpdb->prefix . 'ns WHERE `recipient` = "' . $current_user->user_login . '" AND `read` = 0 AND `deleted` != "2"' );

  if ( empty( $num_pm ) )
  {
    $num_pm = 0;
  }
  if ( empty( $num_unread ) )
  {
    $num_unread = 0;
  }
    $return_array['unreaded'] = $num_unread;
    $return_array['messages_count'] = $num_pm;


  if ( $max )
  {
    $msgs = $wpdb->get_results( 'SELECT `id`, `sender`, `subject`, `read`, `date` FROM ' . $wpdb->prefix . 'ns WHERE `recipient` = "' . $current_user->user_login . '" AND `read` = 0 AND `deleted` != "2" ORDER BY `date` DESC LIMIT ' . $max );
    if ( count( $msgs ) )
    {
      foreach ( $msgs as $msg )
      {
        $msg->sender = $wpdb->get_var( "SELECT display_name FROM $wpdb->users WHERE user_login = '$msg->sender'" );
        $return_array['messages'][] = array(
          'title' => $msg->subject,
          'readed' => $msg->read,
          'username' => $msg->sender,
          'userimage' => 'http://localhost/projects/cendromet/wp-content/themes/cm/assets/img/ui-zac.jpg',
          'when' => $msg->date,
          'url' => wp_nonce_url( "?page=nsfa_inbox&action=view&id=$msg->id", 'rwpm-view_inbox_msg_' . $msg->id ),
        );
      }
    }
  }
  return $return_array;
}
add_action( 'notification_bar', 'nsfa_print_mini_inbox' );

function nsfa_print_mini_inbox(){
  echo '<!-- inbox dropdown start-->
  <li id="header-mini-inbox" class="dropdown">
      <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
          <i class="fa fa-envelope-o"></i>
          <span class="badge bg-theme messages-count"></span>
      </a>
      <ul class="dropdown-menu extended inbox">

      </ul>
  </li>
  <!-- inbox dropdown end -->';
}

function nsfa_get_last_messages(){
  $keyword = trim( strip_tags( $_POST['max'] ) );
	$values = nsfa_get_mini_inbox($keyword);
	die( json_encode( $values ) );
}
