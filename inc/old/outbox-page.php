<?php
/**
 * Outbox page
 */
function nsfa_outbox()
{
    global $wpdb, $current_user;

    // if view message
    if (isset($_GET['action']) && 'view' == $_GET['action'] && !empty($_GET['id'])) {
        $id = $_GET['id'];

        check_admin_referer("rwpm-view_outbox_msg_$id");

        // select message information
        $msg = $wpdb->get_row('SELECT * FROM ' . $wpdb->prefix . 'ns WHERE `id` = "' . $id . '" LIMIT 1');
        $msg->recipient = $wpdb->get_var("SELECT display_name FROM $wpdb->users WHERE user_login = '$msg->recipient'");
        ?>
        <a href="?page=nsfa_outbox" class="btn btn-link btn-sm"><i class="fa fa-angle-left"></i> <?php _e('Back to outbox', 'ns4a'); ?></a>
        <h3><i class="fa fa-angle-right"></i> <?php echo stripcslashes($msg->subject); ?></h3>
        <p><?php printf( __( '<b>Sender</b>: %s<br /><b>Date</b>: %s', 'ns4a' ), $msg->sender, $msg->date ); ?></p>
        <div class="well"><?php echo nl2br( stripcslashes( $msg->content ) ); ?></div>
        <a class="btn btn-danger btn-sm" href="<?php echo wp_nonce_url("?page=nsfa_outbox&action=delete&id=$msg->id", 'rwpm-delete_outbox_msg_' . $msg->id); ?>>"><i class="fa fa-trash-o fa-lg"></i><?php _e( 'Delete', 'ns4a' ); ?>
        </a>
    <?php
        // don't need to do more!
        return;
    }

    // if delete message
    if (isset($_GET['action']) && 'delete' == $_GET['action'] && !empty($_GET['id'])) {
        $id = $_GET['id'];

        if (!is_array($id)) {
            check_admin_referer("rwpm-delete_outbox_msg_$id");
            $id = array($id);
        } else {
            check_admin_referer("rwpm-bulk-action_outbox");
        }
        $error = false;
        foreach ($id as $msg_id) {
            // check if the recipient has deleted this message
            $recipient_deleted = $wpdb->get_var('SELECT `deleted` FROM ' . $wpdb->prefix . 'ns WHERE `id` = "' . $msg_id . '" LIMIT 1');
            // create corresponding query for deleting message
            if ($recipient_deleted == 2) {
                $query = 'DELETE from ' . $wpdb->prefix . 'ns WHERE `id` = "' . $msg_id . '"';
            } else {
                $query = 'UPDATE ' . $wpdb->prefix . 'ns SET `deleted` = "1" WHERE `id` = "' . $msg_id . '"';
            }

            if (!$wpdb->query($query)) {
                $error = true;
            }
        }
        if ($error) {
            $status = array('content' => __('Error. Please try again.', 'ns4a'), 'type' => 'danger');
        } else {
            $status = array('content' =>  _n('Message deleted.', 'Messages deleted.', count($id), 'ns4a'), 'type' => 'success');
        }
    }

    // show all messages
    $msgs = $wpdb->get_results('SELECT `id`, `recipient`, `subject`, `date` FROM ' . $wpdb->prefix . 'ns WHERE `sender` = "' . $current_user->user_login . '" AND `deleted` != 1 ORDER BY `date` DESC');
    ?>
    <h3><i class="fa fa-angle-right"></i> <?php _e('Outbox', 'ns4a'); ?> <span class="badge"><?php echo count($msgs); ?></span></h3>
    <?php
    if (!empty($status)) {
        echo '<div class="alert alert-'.$status['type'].' alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          ', $status['content'], '</div>';
    }
    if (empty($msgs)) {
        echo '<p>', __('You have no items in outbox.', 'ns4a'), '</p>';
    } else {
        ?>
        <form action="" method="get">
            <?php wp_nonce_field('rwpm-bulk-action_outbox'); ?>
            <input type="hidden" name="action" value="delete"/> <input type="hidden" name="page" value="nsfa_outbox"/>

            <section id="no-more-tables">
      				<table class="table table-hover table-striped table-condensed cf" cellspacing="0">
                  <thead>
                  <tr>
                      <th class="manage-column check-column" width="3%" ><input type="checkbox"/></th>
                      <th class="manage-column" width="10%"><?php _e('Recipient', 'ns4a'); ?></th>
                      <th class="manage-column"><?php _e('Subject', 'ns4a'); ?></th>
                      <th class="manage-column" width="20%"><?php _e('Date', 'ns4a'); ?></th>
                      <th class="manage-column" width="10%"></th>
                  </tr>
                  </thead>
                  <tbody>
                      <?php
                      foreach ($msgs as $msg) {
                          $msg->recipient = $wpdb->get_var("SELECT display_name FROM $wpdb->users WHERE user_login = '$msg->recipient'");
                          ?>
                      <tr>
                          <td class="check-column"  data-title="Zaznacz" ><input type="checkbox" name="id[]" value="<?php echo $msg->id; ?>"/>
                          </td>
                          <td data-title="<?php _e('Recipient', 'ns4a'); ?>"><?php echo $msg->recipient; ?></td>
                          <td data-title="<?php _e('Subject', 'ns4a'); ?>">
                              <?php
                              echo '<a href="', wp_nonce_url("?page=nsfa_outbox&action=view&id=$msg->id", 'rwpm-view_outbox_msg_' . $msg->id), '">', stripcslashes($msg->subject), '</a>';
                              ?>
                          </td>
                          <td data-title="<?php _e('Date', 'ns4a'); ?>"><?php echo $msg->date; ?></td>
                          <td class="actions-row" >
                            <a class="btn btn-danger btn-xs" href="<?php echo wp_nonce_url("?page=nsfa_outbox&action=delete&id=$msg->id", 'rwpm-delete_outbox_msg_' . $msg->id); ?>"><i class="fa fa-trash-o fa-lg"></i> <?php _e('Delete', 'ns4a'); ?></a>
                          </td>
                      </tr>
                          <?php

                      }
                      ?>
                  </tbody>
              </table>
          </section>
          <button type="submit" class="btn btn-danger btn-sm" ><i class='fa fa-trash-o fa-lg'></i> <?php _e('Delete Selected', 'ns4a'); ?></button>
        </form>
        <?php

    }
    ?>
</div>
<?php
}
?>
