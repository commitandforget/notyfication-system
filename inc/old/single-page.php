<?php
/*
 * Single page template
 * add <?php do_action( 'main_inbox' ); ?> on template page
 */

//Nav
function nsfa_nav_tabs(){
	?>
	<ul class="nav nav-tabs showback-tabs">
	  <li role="presentation" class="<?php echo ('nsfa_inbox' == $_GET['page'] || !$_GET['page']) ? 'active' : '' ?>"><a href="?page=nsfa_inbox"><?php _e( 'Inbox', 'ns4a' ); ?></a></li>
	  <li role="presentation" class="<?php echo ('nsfa_outbox' == $_GET['page']) ? 'active' : '' ?>"><a href="?page=nsfa_outbox"><?php _e('Outbox \ View Message', 'ns4a'); ?></a></li>
	  <li role="presentation" class="<?php echo ('nsfa_send' == $_GET['page']) ? 'active' : '' ?>"><a href="?page=nsfa_send"><?php _e( 'Send Private Message', 'ns4a' ); ?></a></li>
	</ul>
	<?php
}

function nsfa_main_wrapper(){
  echo '<div class="showback">';
	switch ($_GET['page']) {
		case 'nsfa_outbox':
			nsfa_outbox();
			break;
		case 'nsfa_send':
			nsfa_send();
			break;
		default:
			nsfa_inbox();
			break;
	}
  echo '</div>';
}

add_action( 'main_inbox', 'nsfa_nav_tabs' );
add_action( 'main_inbox', 'nsfa_main_wrapper' );
