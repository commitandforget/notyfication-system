<?php
/**
 * Get
 */
function nsfa_get_notify($only_new = false)
{
	global $wpdb, $current_user;
  $messages = array();
	$num_unread=0;
	$and = '';
	if ($only_new){
		$and = 'AND `read` = 0 AND `deleted` != "2"';
	} else {
		$and = 'AND `deleted` != "2"';
	}
	// show all messages which have not been deleted by this user (deleted status != 2)
	$msgs = $wpdb->get_results( 'SELECT `id`, `message`, `url`, `from`, `date`, `read` FROM ' . $wpdb->prefix . 'ns WHERE `to` = "' . $current_user->ID . '" ' . $and . ' ORDER BY `date` DESC' );

	foreach ( $msgs as $msg )
	{
		$time = strtotime($msg->date);

    $messages[$msg->id]=array(
      'photo'   => NS4A_URL.'/img/'.($msg->from !== '' ? $msg->from : 'default').'.png',
      'from'    => ($msg->from ? $msg->from : 'Inne'),
      'time'    => ($time ? sprintf( _x( '%s ago', '%s = human-readable time difference', 'cm' ), human_time_diff( $time, current_time( 'timestamp' ) ) ) : '') ,
      'message' => $msg->message,
      'url'     => $msg->url,
    );
		if ( !($msg->read) )
		{
			$num_unread++;
		}
	}
  return $messages;
}

/**
 * Add notyfication
 */

function nsfa_add_notify($notyfication_to_add, $debug = flase)
{
    global $wpdb, $current_user;

    $option = get_option( 'nsfa_option' );

		$error = false;
		$status = array();

		// Get input fields with no html tags and all are escaped
		$message = $notyfication_to_add['message'];
    $from = $notyfication_to_add['from'];
    $url =  $notyfication_to_add['url'];
    $date = $notyfication_to_add['date'];
		$to = explode( ',', $notyfication_to_add['to'] );
		$to = array_map( 'strip_tags', $to );

    //Foramt date to mysql
    $date = ($date ? date('Y-m-d H:i:s', $date) : false);

		// Allow to filter content
		$message = apply_filters( 'nsfa_content_send', $message );

		// Remove slash automatically in wp
		$subject = stripslashes( $subject );
		$message = stripslashes( $message );
		$url = stripslashes( $url );
		$date = stripslashes( $date );
		$to = array_map( 'stripslashes', $to );

		// Escape sql
		$subject = esc_sql( $subject );
		$message = esc_sql( $message );
		$url = esc_sql( $url );
		$date = esc_sql( $date );
		$url = esc_sql( $url );
		$to = array_map( 'esc_sql', $to );

		// Remove duplicate and empty recipient
		$to = array_unique( $to );
		$to = array_filter( $to );

		// Check input fields
		if ( empty( $to ) )
		{
			$error = true;
			$status[] = __( 'Please enter username of recipient.', 'ns4a' );
		}

		if ( empty( $message ) )
		{
			$error = true;
			$status[] = __( 'Please enter content of message.', 'ns4a' );
		}

    // If no errors add to database
		if ( !$error )
		{
			$numOK = $numError = 0;
			foreach ( $to as $rec )
			{
				$new_message = array(
					'id'         => NULL,
					'message'    => $message,
					'url'        => $url,
					'from'       => $from,
					'to'         => $rec,
					'date'       => ($date ? $date : current_time( 'mysql' ) ),
					'read'       => 0,
					'deleted'    => 0
				);
				// insert into database
				if ( $wpdb->insert( $wpdb->prefix . 'ns', $new_message, array( '%d', '%s', '%s', '%s', '%s', '%s', '%d', '%d' ) ) )
				{
					$numOK++;
				}
				else
				{
					$numError++;
				}
			}

			$status[] = sprintf( _n( '%d message sent.', '%d messages sent.', $numOK, 'ns4a' ), $numOK ) . ' ' . sprintf( _n( '%d error.', '%d errors.', $numError, 'ns4a' ), $numError );
		}

    //Return

    if ($debug){
      return $status;
    }
    return !$error;
}

/**
 * change status
 */
function nsfa_change_status_notify($id)
{
	global $wpdb;
		if ( !is_array( $id ) ){$id = array( $id );}
		$n = count( $id );
		$id = implode( ',', $id );
		if ( $wpdb->query( 'UPDATE ' . $wpdb->prefix . 'ns SET `read` = "1" WHERE `id` IN (' . $id . ')' ) ){
			$status = true;
		} else {
			$status = false;
		}

    return $status;
}

/**
 * Remove
 */
function nsfa_remove_notify($id)
{

}

/**
 * Ajax
 */
function nsfa_get_last_notifi()
{
  $data = nsfa_get_notify(true);
  $values['messages'] = $data;
  $values['unreaded'] = count($data);
  die( json_encode( $values ) );
}

function nsfa_mar_notifi()
{
  check_ajax_referer( 'nsfa-header-notyfications-once', 'once' );
  $id = explode( ",", trim( strip_tags( $_POST['id'] ) ) );
  $values['status']   = nsfa_change_status_notify($id);
  $values['input']    = $id;
  die( json_encode( $values ) );
}
