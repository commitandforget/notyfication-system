<?php
add_action( 'admin_init', 'nsfa_init' );
add_action( 'admin_menu', 'nsfa_add_menu' );
/**
 * Register plugin option
 *
 * @return void
 */
function nsfa_init()
{
	register_setting( 'nsfa_option_group', 'nsfa_option' );
}

/**
 * Add Option page and PM Menu
 *
 * @return void
 */
function nsfa_add_menu()
{
	global $wpdb, $current_user;

	// // Get number of unread messages
	// $num_unread = $wpdb->get_var( 'SELECT COUNT(`id`) FROM ' . $wpdb->prefix . 'ns WHERE `recipient` = "' . $current_user->user_login . '" AND `read` = 0 AND `deleted` != "2"' );
	//
	// if ( empty( $num_unread ) )
	// 	$num_unread = 0;

	// Option page
	add_options_page( __( 'Notyfications System Options', 'ns4a' ), __( 'Notyfications System', 'ns4a' ), 'manage_options', 'nsfa_option', 'nsfa_option_page' );

	// Add Private Messages Menu
	$icon_url = NS4A_URL . '/img/default.png';
	add_menu_page( __( 'Notyfications System options', 'ns4a' ), __( 'Notyfications System options', 'ns4a' ) , 'manage_options', 'nsfa_options', 'nsfa_option_page', $icon_url );

}

/**
 * Option page
 * Change number of PMs for each group
 */
function nsfa_option_page() {
	?>
<div class="wrap">
	<h2><?php _e( 'Private Messages Options', 'ns4a' ); ?></h2>

	<div style="width:600px;float:left">
		<form method="post" action="options.php">

			<?php
			settings_fields( 'nsfa_option_group' );
			$option = get_option( 'nsfa_option' );
			?>

			<table class="form-table">
				<tr>
					<th><?php _e( 'ID Administratora', 'ns4a' ); ?></th>
					<td>
						<input type="text" name="nsfa_option[administrator_id]" value="<?php echo $option['administrator_id']; ?>"/>
					</td>
				</tr>
			</table>


			<p class="submit">
				<input type="submit" name="submit" class="button-primary" value="<?php _e( 'Save Changes', 'ns4a' ) ?>"/>
			</p>

		</form>

	</div>
</div>
	<?php
}
