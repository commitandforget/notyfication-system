<?php
/**
 * Adding widget
 */
add_action( 'widgets_init', create_function( '', 'return register_widget("NS4A_Widget");' ) );

/**
 * ns4a Widget Class
 */
class NS4A_Widget extends WP_Widget
{

	/**
	 * Constructor
	 */
	function NS4A_Widget()
	{
		$widget_options = array( 'description' => __( 'Show notifications on sidebar', 'ns4a' ) );
		$control_options = array();
		parent::WP_Widget( 'ns4a-widget', __( 'Notifications Widget', 'ns4a' ), $widget_options, $control_options );
	}

	/**
	 * Display widget
	 */
	function widget( $args, $instance )
	{
		global $wpdb, $current_user;

		if ( !is_user_logged_in() )
		{
			return;
		}

		extract( $args );

		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget;

		if ( $title )
		{
			echo $before_title . $title . $after_title;
		}

		$notificationsData = nsfa_get_notify();

		if ( count($notificationsData) )
		{
			echo '<ol>';
			foreach ( $notificationsData as $msg )
			{
				out += '<li>'+
								'<a href="'+obj[value]['url']+value+'">'+
										'<span class="photo"><img alt="avatar" src="'+obj[value]['photo']+'"></span>'+
										'<span class="subject">'+
										'<span class="from">'+obj[value]['from']+'</span>'+
										'<span class="time">'+obj[value]['time']+'</span>'+
										'</span>'+
										'<span class="message">'+
											'<strong>'+obj[value]['message']+'</strong>'+
										'</span>'+
								'</a>'+
						'</li>';


				echo '<li>';
				if ( !$msg->read )
				{
					echo '<b>';
				}
				echo $msg->subject;
				if ( !$msg->read )
				{
					echo '</b>';
				}
				printf( __( '<br />by <b>%s</b><br />at %s', 'ns4a' ), $msg->sender, $msg->date );
				echo '</li>';
			}
			echo '</ol>';
		}

		echo '<p><a href="', get_bloginfo( 'wpurl' ), '/wp-admin/admin.php?page=nsfa_inbox">', __( 'Click here to go to inbox', 'ns4a' ), ' &raquo;</a></p>';

		echo $after_widget;
	}

	/**
	 * Update widget
	 */
	function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['num_pm'] = intval( $new_instance['num_pm'] );
		return $instance;
	}

	function form( $instance )
	{

		/* Set up some default widget settings. */
		$defaults = array( 'title' => __( 'Private Messages', 'ns4a' ), 'num_pm' => 5 );
		$instance = wp_parse_args( (array) $instance, $defaults );
		?>
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
	</p>

	<p>
		<label for="<?php echo $this->get_field_id( 'num_pm' ); ?>"><?php _e( 'Number of PM:', 'ns4a' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'num_pm' ); ?>" name="<?php echo $this->get_field_name( 'num_pm' ); ?>" value="<?php echo $instance['num_pm']; ?>" style="width:100%;" />
	</p>
	<?php

	}
}
