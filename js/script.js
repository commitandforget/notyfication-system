jQuery( document ).ready( function ( $ )
{

	function markAsRead(id){
		console.log(id);
		var ret = $.ajax({
		  method: "POST",
		  url: sitedata.ajaxurl ,
		  data: {
				action: 'nsfa_mar_notifi',
				once: headerNotyficationsTopOnce,
				id: id,
			},
			dataType: "json",
			})
		  .done(function( data ) {
		    return data;
		});
		return ret;
	}
	function formatList(data){
		var out = '';
		var keys = [];
		var obj = data['messages'];
	  for(var key in obj){
	      if(obj.hasOwnProperty(key)){
	          keys.push(key);
	      }
	  }
	  // sort keys
	  keys.sort(function(a, b){return b-a});

		$.each(keys, function( index, value ) {
			out += '<li data-id="'+value+'">'+
							''+
									'<span class="photo"><img alt="avatar" src="'+obj[value]['photo']+'"></span>'+
									'<span class="subject">'+
									'<span class="from">'+obj[value]['from']+'</span>'+
									'<span class="time">'+obj[value]['time']+' <i class="fa fa-circle mar" aria-hidden="true"></i></span>'+
									'</span>'+
									'<a href="'+obj[value]['url']+'"><span class="message">'+
										'<strong>'+obj[value]['message']+'</strong>'+
									'</span></a>'+
							''+
					'</li>';
		});
		return out;
	}

	function updateCounter(newVal){
		var nwe = $( '#header-notifications-orders' );
		nwe.find('.notify-count').text(newVal);
		if (newVal){
			nwe.find('.head').text('Masz '+newVal+' powiadomień');
			nwe.find('.mark-all').show(0);
		} else {
			nwe.find('.head').text('Brak nowych powiadomień');
			nwe.find('.mark-all').hide(0);
		}
	}

	function updateNotifications(){
		var nwe = $( '#header-notifications-orders' );
		$.ajax({
		  method: "POST",
		  url: sitedata.ajaxurl ,
		  data: {
				action: 'nsfa_get_last_notifi',
			},
			dataType: "json",
			})
		  .done(function( data ) {
		    updateCounter(data['unreaded']);
				nwe.find('.messages').html(formatList(data));
		});
	}

	updateNotifications();
	setInterval(updateNotifications, 10000);

	$( '#header-notifications-orders' ).on('click','.mar',function(e){
		e.stopPropagation();
		var id = $(this).closest('li').attr('data-id');
		if(markAsRead(id)){
			$(this).closest('li').slideUp();
			updateCounter(parseInt($( '#header-notifications-orders .notify-count').text())-1);
		} else {
			console.log('false');
			updateNotifications();
		};
	})

	$( '#header-notifications-orders .messages' ).on('click','a',function(e){
		e.stopPropagation();
		var id = $(this).closest('li').attr('data-id');
		if(markAsRead(id)){
			$(this).closest('li').slideUp();
			updateCounter(parseInt($( '#header-notifications-orders .notify-count').text())-1);
			window.location.href = $(this).attr('href');
		} else {
			console.log('false');
			updateNotifications();
			window.location.href = $(this).attr('href');
		};

	})

	$( '#header-notifications-orders' ).on('click','.mark-all',function(e){
		e.stopPropagation();
		var ids = '';
		$( '#header-notifications-orders .messages .mar' ).each(function(){
			ids += $(this).closest('li').attr('data-id') + ',';
		})
		ids = ids.substring(0,(ids.length-1));
		if(markAsRead(ids)){
			$('#header-notifications-orders .messages li').slideUp();
			updateCounter('0');
		} else {
			console.log('false');
			updateNotifications();
		};
	})

} );
