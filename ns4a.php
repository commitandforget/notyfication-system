<?php
/*
Plugin Name: Notyfications system for adminpanel only
Plugin URI:
Description:
Version: 0.9
Author: DevChew based on Rilwis
License: GNU GPL 2+
*/

// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

define( 'NS4A_DIR', plugin_dir_path( __FILE__ ) );
define( 'NS4A_INC_DIR', trailingslashit( NS4A_DIR . 'inc' ) );

define( 'NS4A_URL', plugin_dir_url( __FILE__ ) );
define( 'NS4A_CSS_URL', trailingslashit( NS4A_URL . 'css' ) );
define( 'NS4A_JS_URL', trailingslashit( NS4A_URL . 'js' ) );


include_once NS4A_INC_DIR . 'operations.php';
include_once NS4A_INC_DIR . 'draw-notify.php';
include_once NS4A_INC_DIR . 'create-system-notify.php';
//include_once NS4A_INC_DIR . 'widget.php';

if ( is_admin() )
{
	include_once NS4A_INC_DIR . 'options.php';
}

register_activation_hook( __FILE__, 'nsfa_activate' );
add_action( 'plugins_loaded', 'nsfa_load_text_domain' );

add_action( 'wp_ajax_nsfa_get_last_notifi', 'nsfa_get_last_notifi' );
add_action( 'wp_ajax_nsfa_mar_notifi', 'nsfa_mar_notifi' );

function nfsa_scripts(){
	wp_register_script( 'nsfa_script', plugins_url( '/js/script.js' , __FILE__ ), array( 'jquery' ) );
	wp_localize_script( 'nsfa_script', 'sitedata', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'homeurl' => get_home_url()
		) );
	wp_enqueue_script( 'nsfa_script' );
}

add_action( 'wp_enqueue_scripts', 'nfsa_scripts' , 120);

/**
 * Load plugin text domain
 *
 * @return void
 */
function nsfa_load_text_domain()
{
	load_plugin_textdomain( 'ns4a', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
}

/**
 * Create table and register an option when activate
 *
 * @return void
 */
function nsfa_activate()
{
	global $wpdb;

	// Create table
	$query = 'CREATE TABLE IF NOT EXISTS ' . $wpdb->prefix . 'ns (
		`id` bigint(20) NOT NULL auto_increment,
		`message` text NOT NULL,
		`url` text NOT NULL,
		`from` varchar(60) NOT NULL,
		`to` varchar(60) NOT NULL,
		`date` datetime NOT NULL,
		`read` tinyint(1) NOT NULL,
		`deleted` tinyint(1) NOT NULL,
		PRIMARY KEY (`id`)
	) COLLATE utf8_general_ci;';

	// Note: deleted = 1 if message is deleted by sender, = 2 if it is deleted by recipient

	$wpdb->query( $query );

	// Default numbers of PM for each group
	$default_option = array(
		'administrator_id' => 1,
	);
	add_option( 'nsfa_option', $default_option, '', 'no' );
}
