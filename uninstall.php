<?php
global $wpdb;

// Drop PM table and plugin option when uninstall
$wpdb->query( "DROP table {$wpdb->prefix}ns" );
delete_option( 'nsfa_option' );
